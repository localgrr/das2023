<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );  
add_shortcode('get_related_events_sidebar', 'das_people::get_related_events_sidebar'); 

function enqueue_parent_styles() {

	wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri().'/css/theme.css' );
	wp_enqueue_script( 'das2023-bootstrap', get_stylesheet_directory_uri() . '/js/theme-bootstrap4.min.js', array(), 1, true );
	wp_enqueue_script( 'das2023-scripts', get_stylesheet_directory_uri() . '/js/theme.js', array(), 2, true );
}

add_action( 'admin_enqueue_scripts', 'load_admin_style' );

function load_admin_style() {
    wp_enqueue_style( 'admin_css', get_stylesheet_directory_uri() . '/css/admin.css', false, '1.0.0' );
}

/**
 * Enable shortcodes for menu navigation.
 */
if ( ! has_filter( 'wp_nav_menu', 'do_shortcode' ) ) {
    add_filter( 'wp_nav_menu', 'shortcode_unautop' );
    add_filter( 'wp_nav_menu', 'do_shortcode', 11 );
}

function pre_r($var) {

	echo "<pre>";
	print_r($var);
	echo "</pre>";
	
}
 
show_admin_bar(false); 

add_filter('og_og_description_meta', 'my_og_og_description_meta');
function my_og_og_description_meta($description)
{
	if(get_field('seo_description')){ //if the field is not empty
		return '<meta property="og:description" content="' . get_field('og_description') . '">'; 
	}
    return $description;
}

function hide_organizers_section() {
    if (tribe_is_event_edit() || tribe_is_event_create()) {
        remove_action('tribe_events_organizer_before_metabox', 'tribe_events_meta_organizer_section');
    }
}
add_action('tribe_events_event_form_before_template', 'hide_organizers_section');

include "inc/das_acf.php";
include "inc/das_people.php";
add_shortcode('get_teams', array("das_people","get_teams"));
add_shortcode('das_instructors', array("das_people","get_instructors"));

include "inc/das_events.php";
include "inc/das_carousel.php";
include "inc/svg_support.php";

das_events::init();

$carousel = new das_carousel();
add_shortcode('das_carousel', array( $carousel, 'init' ) ); 
     
?>