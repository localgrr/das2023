# README #

This is a Wordpress Child theme for Das2023. It is a child of the Understrap theme
You need to install (and not activate) this theme from https://wordpress.org/themes/understrap/ for the DAS theme to work
Get the files from here https://github.com/understrap/understrap

The themes folder structure should be:

wp-content
	themes
		understrap
		das2023

## Push to deploy

The [`WP-Pusher`](https://wppusher.com/) plugin can be installed to trigger theme updates when new code is pushed to a branch of this repository. You can link different branches to different instances, for example a staging server can be linked to the `main` branch while the production server is linked to a `production` branch.