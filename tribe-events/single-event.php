<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

$cat = wp_get_post_terms($event_id, 'tribe_events_cat')[0]->name;


?>

<div id="tribe-events-content" class="tribe-events-single">

	<!-- Notices -->
	<?php tribe_the_notices() ?>

	<?php the_title( '<h1 class="tribe-events-single-event-title ' . $cat .' jij">', '</h1>' ); 

	echo das_events::get_ticket_button_html( $event_id, $cat);

	?>

	<div class="tribe-events-schedule tribe-clearfix">
		<?php echo tribe_events_event_schedule_details( $event_id, '<h2>', '</h2>' ); ?>
		<?php if ( tribe_get_cost() ) : ?>
			<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
		<?php endif; ?>
	</div>

	<!-- #tribe-events-header -->

	<?php while ( have_posts() ) :  the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<!-- Event featured image, but exclude link -->
			<div class="row">
				<div class="col-md-3">
					<?= das_events::get_any_image( get_the_id(), "large", true ); ?>
				</div><!-- .col-md-3 -->
				<div class="col-md-9">
					<div class="tribe-events-single-event-description tribe-events-content">
						<?php das_events::the_content(); ?>
					</div>
				</div>
				<div class="col-12">
					<?php tribe_get_template_part( 'modules/meta' ); 

					das_people::get_socials();
					das_people::print_participants( $event_id );
					das_people::get_all_related_events();
					?>
				</div><!-- .col-12 -->
			</div>
			
		</div> <!-- #post-x -->
	<?php endwhile; ?>

</div><!-- #tribe-events-content -->