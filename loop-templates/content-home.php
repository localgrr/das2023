<?php
/**
 * Partial template for content in home.php
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="entry-content row">

		<div class="col-md-4">
			<?= das_events::get_any_image(get_the_id(), "medium", true); ?>
			
			</div>
		<div class="col-md-8">
			<?= das_events::get_excerpt(get_the_id(), 100); ?>
		</div>
		<div class="col-12">
			<?= das_people::get_socials(); das_people::get_related_events(); ?>
		</div>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php edit_post_link( __( 'Edit', 'understrap' ), '<span class="edit-link">', '</span>' ); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
