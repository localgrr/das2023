<?php
/**
 * Single person partial template
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title text-popping">', '</h1>' ); ?>

	</header><!-- .entry-header -->


    <div class="row">
        <div id="image" class="person-headshot col-md-4">
            <?= das_events::get_any_image(get_the_id(), "large", true); ?>
        </div>

        <div class="col-md-8">
            <div class="entry-content">

                <?php
                the_content();
                understrap_link_pages();
                ?>

            </div><!-- .entry-content -->

            <?= das_people::get_socials(); das_people::get_related_events(); ?>
        </div>

    </div>

	<footer class="entry-footer">

		<?php understrap_entry_footer(); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->
