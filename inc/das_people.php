<?php
if ( ! class_exists( 'das_people' ) ) {

	class das_people {

		public static $social_obj;

/**
 * Get all the participants of this event from the ACF field
 * 
 * This function reads the ACF people field and returns all the members of
 * that team, or if no team was assigned (eg. it is a class) returns participant(s)
 *
 * @param int $id WP page ID
 * @return array containing image, role and name of all participants
 * 
 */	
		static function get_all_participants($id) {

			$participants = self::get_participants($id);

			if(empty($participants)) return false;

			$all_participants = [];

			foreach ($participants as $p) {

				if(isset($p["individual"]->ID)) {

					$p["individual"]->thumbnail = get_the_post_thumbnail_url( $p["individual"]->ID, 'thumbnail' );
					array_push($all_participants, $p["individual"]);

				}

				if(isset($p["team"]->ID)) {

					$arr = get_post($p["team"]->ID);
					$arr->role = "Featured Team";
					$arr->thumbnail = das_events::get_any_image(( $p["team"]->ID), "thumbnail");
					array_push($all_participants, $arr);

					$members = get_field("members", $p["team"]->ID);
					
					foreach ($members as $m) {
						if($m["member"]) {
							$m["member"]->role = $m["role"];
							$m["member"]->thumbnail = get_the_post_thumbnail_url( $m["member"]->ID, 'thumbnail' );
							array_push($all_participants, $m["member"]);	
						}
					}
				}
			}

			return $all_participants;
		}

/**
 * Print the HTML for event participants using the participants array
 * 
 *
 * @param int $id WP page ID
 * @return void but echos the participants HTML
 * 
 */	
		static function print_participants($id) {

			$all_participants = self::get_all_participants($id);

			if(empty($all_participants)) return false;

			$ht = '<h3 class="das-participants-list-header">Featuring:</h3>
			<ul class="das-participants-list list-inline">';

			foreach ($all_participants as $p) {

				$ht .= $p->post_type == "team" ? '<br>' : '' ;
				
				$ht .= '
				<li class="das-participant list-inline-item">
					<a href="' . $p->guid . '">
						<img src="' . $p->thumbnail . '">	
						<p class="das-participant-title">' . $p->post_title . '</p>
					</a>
				</li>';
			}

			$ht .= '</ul>';

			echo $ht;

		}

/**
 * Return the participants stored in the ACF field "people"
 * 
 * @param int $id WP page ID
 * @return array containing the ACF people field associated with the page ID
 * 
 */	
		static function get_participants($id) {

			$people = get_field("people", $id) ? get_field("people", $id) : get_field("members", $id);

			return $people;

		}

/**
 * Return the list of participants as a simple comma separated list
 * 
 * @param int $id WP page ID
 * @return string containing names of the participants separated with a comma
 * 
 */	
		static function list_event_participants($id) {

			$participants = self::get_participants($id);

			$arr = [];

			foreach ($participants as $p) {
				if($p["team"]) array_push($arr, $p["team"]->post_title);
				if($p["individual"]) array_push($arr, $p["individual"]->post_title);
			}

			return implode(", ", $arr);

		}


/**
 * Return the persons social media
 * 
 * @param int $id WP person ID
 * @return string HTML containing all of that persons social media
 * 
 */	
		static function get_socials($id = null) {

			$id = $id ? $id : get_the_ID();
			$post_type = get_post_type($id) == 'person' ? 'individual' : 'team'; 

			self::$social_obj = $socials = ($post_type == 'individual') ? get_field_object('social_media', $id)["value"] : get_field_object('social_media', $id)["value"]["social_media"];

			if($id and $socials and strlen(implode(array_values($socials))) > 0 ) {
				return self::print_social_media($id);
			}
				
			return false;
		}

/**
 * return the HTML for a social media link
 * 
 * @param string $name name of social media item in ACF social_media object
 * @return string HTML containing the social media link
 * 
 */	

		static function get_social_media_link($name, $link) {

			if(!$link) return false;

			$link = ($name == "email") ? "mailto:" . $social_link : $link;
			$label = self::get_social_label_from_name($name);

			return  '<a href="' . $link . '" class="social-media-link col-md-3"><span alt="' . $label . ' logo" class="social-media-icon social-media-' . $name .'"></span><span>' . $label . '</span></a>';

		}
 
/**
 * Print social media HTML
 * 
 * @param int $id WP person ID
 * @return string HTML containing all of that persons social media
 * 
 */	
		static function print_social_media($id) {


			if(empty(self::$social_obj)) return false;

            $name = get_the_title($id);
            
			$ht = '	<div id="social-media">
						<h4 class="text-popping">Social media</h4> <p>You can follow ' . $name . ' on social media at:</p>
							<div id="social-links" class="row">';

			foreach(self::$social_obj as $name => $social_link) {

				if($name == "other_social_media") {

					foreach($social_link as $social_arr) {

						$ht .= self::get_social_media_link($social_arr["name"], $social_arr["url"]);

					}


				} else {

					$ht .= self::get_social_media_link($name, $social_link);

				}

			}

			$ht .= '</div></div>';

			return $ht;
		}


/**
 * Return the label of a social media array item from the ACF object social_media 
 * 
 * @param string $name name of social media item 
 * @return string containing label name
 * 
 */	
		static function get_social_label_from_name($name) {

            $fields = self::$social_obj["sub_fields"];

            foreach($fields as $field) {

            	if($field["name"] == $name) return $field["label"];

            }

            return $name;

		}

/**
 * Get related events for every person attach ed to this post
 * 
 * @param int WP post ID
 * @return string containing HTML of related events
 * 
 */	
		static function get_all_related_events($attr = null, $id = null) {

			$id = $id ? $id : get_the_ID();

			$people = self::get_all_participants($id);

			$get_all_related_events = $related_workshops = $related_shows = [];

			foreach($people as $person) {

				array_push($get_all_related_events, self::get_related_events($person->ID, false));

			}

			foreach($get_all_related_events as $event) {

				if($event["workshops"]) {

					foreach($event["workshops"] as $workshop) {

						array_push($related_workshops, $workshop);

					}
				}

				if($event["shows"]) {

					foreach($event["shows"] as $show) {

						array_push($related_shows, $show);
							
					}

				}

			}

			function comparePosts($post1, $post2) {

			    return $post1->ID - $post2->ID;

			}

			usort($related_shows, 'comparePosts');
			$related_shows = array_unique($related_shows, SORT_REGULAR); //remove dupes
			usort($related_shows, "self::compare_dates"); //order by date

			usort($related_workshops, 'comparePosts');
			$related_workshops = array_unique($related_workshops, SORT_REGULAR); //remove dupes
			usort($related_workshops, "self::compare_dates"); //order by date


			$arr["shows"] = $related_shows;
			$arr["workshops"] = $related_workshops;

			echo self::get_related_events_html($arr);

		}

	static function get_related_events_sidebar($id = null, $echo = true) {

		self::get_related_events(get_the_ID(), true, true);

	}

/**
 * Get all the events related to this person or team
 * 
 * @param int WP people or team ID
 * @return string containing HTML of related events
 * 
 */	
		static function get_related_events($id = null, $echo = true, $minimal = false, $event_type = "all") {

            $id = $id ? $id : get_the_ID();

			$args = ([
				'posts_per_page' => -1
			]); 

			$events = tribe_get_events($args);

			$related_events = array(
				"shows" => [],
				"workshops" => []
			);

			$post_type = get_post_type($id);

			foreach($events as $i => $event) {

				$people = get_field("people", $event->ID);

				foreach($people as $person) {
					
					if(isset($person["team"]->ID)) {

						if($event_type == "workshop") continue;

						//is this person in the team related to this event
						if(self::team_search($id, $person["team"]->ID)) array_push($related_events["shows"], $event);

						//or this is a team and are they attached to this event?
						if($person["team"]->ID == $id) array_push($related_events["shows"], $event);

					} else {

						if($event_type == "show") continue;

						//is this person in the team related to this event
						if(self::team_search($person["individual"]->ID, $id)) array_push($related_events["workshops"], $event);

						//or this is a team and are they attached to this event?
						if($person["individual"]->ID == $id) array_push($related_events["workshops"], $event);

					}

				}

			}

			if($related_events["shows"]) usort($related_events["shows"], "self::compare_dates");
			if($related_events["workshops"]) usort($related_events["workshops"], "self::compare_dates");

			if($echo) {

				echo self::get_related_events_html($related_events, $minimal);

			} else {

				return $related_events;

			}
;
		}

static function get_related_events_list_html($related_events, $minimal = false, $type = "all") {

	$text = ($type == "workshop") ? "Workshops" : "Related Events";

	$ht = '<h3 class="related-events-list-header h5 mt-3">' . $text . '</h3><ul class="related-events-list list-unstyled">';

	
		if(isset($related_events["shows"][0]) && $type !== "workshop") {

			//dont show related shows if its there's only one related show and this is the page for it
			if( (count($related_events["shows"]) > 1) || ($related_events["shows"][0]->ID != get_the_ID()) ) {

				foreach($related_events["shows"] as $show) {
					$link = $show->guid;
					$ht .= '
					<li class="related-event-item mb-4">
						<h4 class="h5"><a href="' . $show->guid . '">' . $show->post_title . '</a></h4>
						<p class="related-event-item-date">' . das_events::format_day_header($show->event_date) . ' @ ' . das_events::format_event_list_date($show->event_date) . '</p>
						<a class="btn btn-primary related-event-item-btn related-event-item-btn-more mr-4" href="' . $link . '">More Info</a>';

						//$url = tribe_get_event_external_url($event);
						$url =  get_post_meta($show->ID, '_EventURL', true);
						if($url) {
							$ht .=das_events::get_ticket_button_html($show->ID,"Show");
						}
						$ht .='
					</li>';

				}

			}

		}

		if(isset($related_events["workshops"][0]) && $type !== "show") {

			//dont show related shows if its there's only one related show and this is the page for it
			if( (count($related_events["workshops"]) > 1) || ($related_events["workshops"][0]->ID != get_the_ID()) ) {

				foreach($related_events["workshops"] as $show) {

					$ht .= '
					<li class="related-event-item mb-4">
						<h4 class="h5"><a href="' . $show->guid . '">' . $show->post_title . '</a></h4>
						<p class="related-event-item-date">' . das_events::format_day_header($show->event_date) . ' @ ' . das_events::format_event_list_date($show->event_date) . '</p>
						<a href="' . $show->guid . '" class="btn btn-primary related-event-item-btn related-event-item-btn-more mr-4">More Info</a>';

						//$url = tribe_get_event_external_url($event);
						$url =  get_post_meta($show->ID, '_EventURL', true);
						if($url) {
							$ht .= das_events::get_ticket_button_html($show->ID,"Workshop");
							
						}
						$ht .='
					</li>';

				}

			}

		}

	$ht .= '</ul>';

	return $ht;

}

/**
 * Return the related events HTML
 * 
 * @param arr $related_events
 * @return str HTML containing the related events
 * 
 */	
static function get_related_events_html($related_events, $minimal = false, $show_me = false) {

	$minimal_class = $minimal ? ' minimal' : '';

	$ht = '';

	if(isset($related_events["shows"][0])) {

		//dont show related shows if its there's only one related show and this is the page for it
		if( (count($related_events["shows"]) > 1) || ($related_events["shows"][0]->ID != get_the_ID()) ) {

			$ht .= '<h3>Related Shows</h3>

			<ul class="related_events list-unstyled' . $minimal_class . '">';

			foreach($related_events["shows"] as $show) {

				$ht .= self::get_related_event_item_html($show, "Show");

			}

			$ht .= '</ul>';

		}

	}

	if(isset($related_events["workshops"][0])) {

		//dont show related workshops if its there's only one related workshop and it's this event for it
		if( (count($related_events["workshops"]) > 1) || ($related_events["workshops"][0]->ID !== get_the_ID()) || $show_me) {

			$ht .= '<h3>Related Workshops</h3>
			<ul class="related_events list-unstyled">';

			foreach($related_events["workshops"] as $workshop) {

				$ht .= self::get_related_event_item_html($workshop, "Workshop");

			}

			$ht .= '</ul>';

		}

	}

	return $ht;
}

/**
 * Return the related events item HTML
 * 
 * @param obj $event
 * @return str HTML containing the event item
 * 
 */	
static function get_related_event_item_html($event, $type = "Show", $show_this_event = false) {

	if( ($event->ID == get_the_ID()) && (!$show_this_event) ) return false;

	$img = das_events::get_any_image($event->ID, "thumbnail", true);

	$ht .= '
	<li class="related-event-item mb-4">
		<h4><a href="' . $event->guid . '">' . $event->post_title . '</a></h4>
		<div class="row">
			<div class="col-sm-2 image">' . $img . '</div>
			<div class="col-sm-10">
				<p class="related-event-item-date">' . das_events::format_day_header($event->event_date) . ' @ ' . das_events::format_event_list_date($event->event_date) . '</p>
				<p class="related-event-item-content">' . das_events::trim(das_events::the_content($event->ID, false)) . '</p>

				<a href="' . $event->guid . '" class="btn btn-primary related-event-item-btn related-event-item-btn-more mr-4">More Info</a>';

				//$url = tribe_get_event_external_url($event);
				$url =  get_post_meta($event->ID, '_EventURL', true);

				if($url) {
					$ht .=
					das_events::get_ticket_button_html($event->ID,$type);
				}
				$ht .='
			</div>
		</div>
	</li>';

	return $ht;

}

/**
 * Compare these date for usort
 * 
 * @param date $date1
 * @param date $date2
 * @return bool
 * 
 */	
static function compare_dates($date1, $date2) {
    $timestamp1 = strtotime($date1->event_date);
    $timestamp2 = strtotime($date2->event_date);

    if ($timestamp1 === $timestamp2) {
        return 0;
    }

    return ($timestamp1 < $timestamp2) ? -1 : 1;
}

/**
 * Is this person on this team?
 * 
 * @param int WP people ID
 * @param int WP team ID
 * @return bool
 * 
 */	
	static function team_search($person_id, $team_id) {

		$members = get_field( "members", $team_id );

		foreach( $members as $member) {

			if($member["member"]->ID == $person_id) return true;

		}

		return false;
	}

	static function get_instructors($atts) {

		$args = array(
		    'post_type'      => 'tribe_events',
		    'posts_per_page' => -1,  // Retrieve all events
		    'tax_query'      => array(
		        array(
		            'taxonomy' => 'tribe_events_cat',
		            'field'    => 'slug',
		            'terms'    => 'workshop',
		        ),
		    ),
		);

		$events = tribe_get_events($args);

		foreach ($events as $i => $event) {
			$events[$i]->people = get_field("people", $event->ID);
		}

		$args = array(
		    'post_type'      => 'person',
		    'posts_per_page' => -1,
		    'post_status' 	=> ['publish','private']
		);

		$people = get_posts($args);

		$instructors = [];

		$ids = [];

		$ht = '<div class="das-instructors das-catalog-list">';

		foreach ($people as $person) {

			foreach ($events as $event) {

				foreach ($event->people as $event_person) {


					if(isset($event_person["individual"]->ID)) {

						if($person->ID == $event_person["individual"]->ID) {

							if (in_array($person->ID, $ids)) continue;

							array_push($ids, $person->ID);

							$related_events = self::get_related_events($person->ID, false, true, "workshop");

				            $img = '<div class="col-md-4">' . das_events::get_any_image($person->ID, "medium", true) . '</div>';
				            $text = '<div class="col-md-8">' . das_events::get_excerpt($person->ID, 400) . self::get_related_events_list_html($related_events, true, "workshop") . '</div>';

							$ht .= '
								<div class="row">
									<h3 class="text-popping h1 col-12"><a href="' . $person->guid . '">' . $person->post_title . '</a></h3>';

							$ht .= $img . $text;

							$ht .= '</div>';

						}

					}
					
				}
				
			}
			
		}

		return $ht;

	}


	static function get_teams($atts) {

		$default_atts = array(
		    'post_type'      => 'team',
		    'posts_per_page' => -1,
			'orderby'        => 'title',
			'order'          => 'ASC',
			'headliner' => null,
			'length' => 100
		);

		$atts = array_merge($default_atts, $atts);

	    ob_start();

	    $query = new WP_Query($atts);

	    $ht = '<div class="das_teams das-catalog-list" id="headliners">';

	    if ($query->have_posts()) {
	        while ($query->have_posts()) {
	            $query->the_post();

	            $headline_field = get_field("headliner");

	            if( ($atts["headliner"] == "only") && !$headline_field) continue;
	            if( ($atts["headliner"] == "omit") && $headline_field) continue;

	            
	            $related_events = self::get_related_events(get_the_id(), false, true);

	            $img = '<div class="col-md-4">' . das_events::get_any_image(get_the_id(), "medium", true) . '</div>';
	            $text = '<div class="col-md-8">' . das_events::get_excerpt(get_the_id(), $atts["length"]) . self::get_related_events_list_html($related_events) . '</div>';

				$ht .= '
					<div class="row">
						<h3 class="text-popping h1 col-12"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h3>';

				$ht .= $img . $text;

				$ht .= '</div><!-- .row -->';

	        }

	    }

	    $ht .= '</div>';

	    wp_reset_postdata();

	    ob_get_clean();

	    return $ht;

	}

}

}

?>