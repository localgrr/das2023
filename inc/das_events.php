<?php
if ( ! class_exists( 'das_events' ) ) {

	class das_events {

		CONST human_day_format = "l F j";
		CONST human_event_list_format = "g:ia";
		CONST home_page_id = 80;

		public static $venue;

		public static $cat;

		public static $no_filter;

		public static $das_signup_link;

		public static $default_ticket_link;


/**
 * Initial function to run
 *
 * This function grabs and stores the signup form link from the home page ACF
 * it also sets up the WP shortcodes
 *
 * @return void 
 * 
 */		
		static function init() {

			self::$das_signup_link = get_field('signup_form_link', 'option');

			self::$default_ticket_link = get_field('default_ticket_link', 'option');

			add_shortcode('das_list_events', 'das_events::das_list_events'); 
			add_shortcode('das_signup_link', 'das_events::das_signup_link'); 
			add_shortcode('das_signup_link_html', 'das_events::das_signup_link_html'); 

		}

/**
 * Returns an HTML containing the signup form URL
 *
 * @param string $text a string for the link text
 * @return string HTML link 
 * 
 */	
		static function das_signup_link_html($text) {

			$text = $text ? $text : "Apply Now";

			return ('<a href="' . self::$das_signup_link . '">' . $text . '</a>');

		}

/**
 * Returns a string containing the signup link URL
 *
 * @return string URL of signup link 
 * 
 */
		static function das_signup_link() {

			return self::$das_signup_link;

		}

		static function is_headliner($event_id) {

			$team_id = isset(get_field("people", $event_id)[0]["team"]->ID) ? get_field("people", $event_id)[0]["team"]->ID : false;

			if(!$team_id) return false;


			return (get_field("headliner", $team_id) == true) ? true : false;

		}

/**
 * Groups Tribe events by day
 * 
 * This function takes the Tribe events from the database, and adds keys for day,
 * venue and venue so they can be filtered more easily. It then sorts the
 * array by day as this is just a 3 day festival.
 * If filters already exist in the querystring, just return those events.
 * Useful so people can bookmark views
 *
 * @return array Tribe events grouped by day
 * 
 */
		static function group_tribe_events_by_day() {

			//are there already filters in the querystring? Then filter the events
			if (isset($_GET['venue'])) self::$venue = $_GET['venue'];
			if (isset($_GET['cat'])) self::$cat = $_GET['cat'];

			//this flag helps set the css classes correctly
			if(!isset(self::$venue) && !isset(self::$cat)) self::$no_filter = true;

			$args = ([
				'eventDisplay' => 'list',
				'posts_per_page' => -1
			]); 

			if(self::$venue) $args["venue"] = self::$venue;
			if(self::$cat) $args["event_category"] = self::$cat;

			$events = tribe_get_events($args); 

			//add extra info we need
			foreach ($events as $i => $event) {
				$events[$i]->day = substr($events[$i]->event_date, 0, 10);
				$events[$i]->venue = tribe_get_venue( $events[$i]->ID );
				$events[$i]->categories = tribe_get_event_cat_ids( $events[$i]->ID );
				$events[$i]->headliner = self::is_headliner( $events[$i]->ID );
			}

			$arr = array();

			foreach ($events as $key => $item) {
			   $arr[$item->day][$key] = $item;
			}

			ksort($arr, SORT_NUMERIC);

			return $arr;
		}

/**
 * Make the day heading look readable
 *
 * @param string $date a string in date format
 * @return string formatted date
 * 
 */	
		static function format_day_header($date) {

			return date(self::human_day_format, strtotime($date)); 

		}

/**
 * Make the inline event date look readable
 *
 * @param string $date a string in date format
 * @return string formatted date
 * 
 */	
		static function format_event_list_date($date) {

			return date(self::human_event_list_format, strtotime($date));

		}

/**
 * Return css classes that show which venue and category are relevant to this event
 * 
 * @param obj $obj Tribe event object
 * @return string css classes related to this event
 * 
 */	
		static function get_event_css_classes($obj) {

			$venue = "venue-" . sanitize_title($obj->venue);
			$cats = "cat-" . implode(" cat-", $obj->categories);
			$headliner = $obj->headliner ? " headliner" : "";

			return $venue . " " . $cats . $headliner; 

		}

/**
 * Echo the ticket button HTML
 * 
 * @param int $id WP page ID
 * @return void
 * 
 */	
		static function print_ticket_button($id, $cat = "Workshop") {

			echo self::get_ticket_button_html($id, $cat = "Workshop");

		}

		static function get_stock_ccb_stock_quantity($product_id) {

			// Your WooCommerce API credentials
			$api_url = 'https://training.comedycafeberlin.com/wp-json/wc/v3/products/';
			$consumer_key = get_field('ccb_api_ck', 'option');
			$consumer_secret = get_field('ccb_api_tk', 'option');

			$complete_api_url = $api_url . $product_id;

			$options = array(
			    'http' => array(
			        'header'  => "Authorization: Basic " . base64_encode("$consumer_key:$consumer_secret")
			    )
			);

			$context = stream_context_create($options);

			// Execute HTTP request and get the response
			$response = file_get_contents($complete_api_url, false, $context);

			if ($response === FALSE) {
			    die('Error occurred');
			}

			// Decode JSON response
			$product_data = json_decode($response, true);

			// Check the inventory level
			if (isset($product_data['stock_quantity'])) return $product_data['stock_quantity'];

			return false;

		}


/**
 * Build and return the ticket button HTML
 * 
 * @param int $id WP page ID
 * @return string button HTML
 * 
 */	
static function get_ticket_button_html($id, $cat = "Workshop") {

	if(get_post_meta( $id, "sold_out", true ) == 1) {
		return '<a href="javascript:void(0)" class="btn btn-light event-ticket disabled ' . $cat . '">Sold out!</a>';
	}

	$url = get_post_meta( $id, "_EventURL" );

	$url = $url[0] ? $url[0] : self::$default_ticket_link;

	$text = $cat == "Show" ? "Get festival pass" : "Register";

	$check = $cat == "Show" ? "shows_on_sale" : "workshops_on_sale";

	if(!get_field($check, 'option')) {
		return '<a href="javascript:void(0)" class="btn btn-light event-ticket disabled ' . $cat . '">Available soon!</a>';
	} 
	else {
		return '<a href="' . $url . '" class="btn btn-primary event-ticket ' . $cat . '">' . $text .'</a>';
	}
}

/**
 * Print whatever content you can find
 * 
 * @param int $id WP page ID
 * @return str HTML containint content
 * 
 */	
		static function the_content($id = null, $echo = true) {

			$id = $id ? $id : get_the_ID();

			$content = get_post_field('post_content', $id);

			if(!$content) {

				//if there's no content for this page search for teams

				$id = get_field("people", $id)[0]["team"]->ID;

				if(isset($id)) {

					$field = get_post_field('post_content', $id);
					if(!$echo) return $field;
					echo $field;

				}

				//pre_r($fields);

			} else {

				if(!$echo) return $content;
				echo $content;
				
			}

		}


/**
 * Build and return the event filters
 * 
 * @param int $id WP page ID
 * @return void
 * 
 */	
		static function get_event_filters() {

			$venues = get_posts( [
			    'numberposts'      => -1,
			    'post_type'        => 'tribe_venue'
			]);

			$terms = get_terms('tribe_events_cat', array(
			    'hide_empty' => false,
			));


			$output = '
			<div class="btn-toolbar filter-shows" role="toolbar" aria-label="Filter shows">
			<div data-toggle="buttons" class="toggle-buttons">
			<form class="event-filter-form row" method="get">
			<div class="btn-group" role="group" aria-label="Venues">
			<div class="btn-group-inner">
			<label class="btn-group-label">Venue</label>';
			foreach ($venues as $i => $venue) {

				if(self::$no_filter || !isset(self::$venue)) {

					$checked = " checked";
					$checked_class = " active";

				} else {

					$checked = in_array($venue->ID, self::$venue) ? " checked" : "";
					$checked_class = in_array($venue->ID, self::$venue) ? " active" : "";

				} 

				$output .= '<label class="btn ' . $checked_class . '" for="btn-check"><i class="icon"></i>
							' . $venue->post_title . '<input type="checkbox" name="venue[]" value="' .  $venue->ID . '" autocomplete="off" ' . $checked . '></label>';
			}
			$output .= '
			</div><!-- .btn-group-inner -->
			</div><!-- .btn-group -->
			<div class="btn-group" role="group" aria-label="Type">
			<div class="btn-group-inner">
			<label class="btn-group-label">Event type</label>';

			foreach ($terms as $i => $term) {

				if(self::$no_filter || !isset(self::$cat)) {

					$checked = " checked";
					$checked_class = " active";

				} else {

					$checked = in_array($term->term_taxonomy_id, self::$cat) ? " checked" : "";
					$checked_class = in_array($term->term_taxonomy_id, self::$cat) ? " active" : "";

				}


				$output .= '<label class="btn' . $checked_class . '" for="btn-check"><i class="icon"></i><input type="checkbox" name="cat[]" value="' .  $term->term_taxonomy_id . '" autocomplete="off" ' . $checked . '>
							' . $term->name . '</label>';
			}
			$output .= '

			</div><!-- .btn-group-inner -->
			</div><!-- .btn-group -->
			</form>
			</div><!-- /toggle -->
			</div><!-- .btn-toolbar -->';

			return $output;
		}

		static function print_event_image($post_id) {

			echo self::get_any_image($post_id);

		}

		static function trim($content, $length = 100) {

			$content = strip_shortcodes($content);
			$content = strip_tags($content);

			$total_length = strlen($content);

			if($length > $total_length) return $content;

			//find last space within length
			$last_space = strrpos(substr($content, 0, $length), ' ');

			if($last_space !== false) {

				$trimmed_text = substr($content, 0, $last_space);

			} else {

				$trimmed_text = substr($content, 0, $length);

			}

			$trimmed_text .= "...";

			return $trimmed_text;

		}

		static function get_excerpt($id, $length = 100) {

			$content = get_post_field('post_content', $id);
			$content = strip_shortcodes($content);
			$content = strip_tags($content);

			$content_length = strlen($content);

			if($content_length == 0) {

				$people = das_people::get_participants($id);

				foreach($people as $i => $p) {

					$key = (isset($p["team"]->post_content)) ? "team" : "individual";

					$content .= $p[$key]->post_content;

					$content_length = strlen($content);

					if ($i !== array_key_last($people)) {

						$content .= "<br><br>";

					}
				}

			}

			$excerpt = wp_trim_words( $content, $length,'' );

			if(strlen($excerpt) < $content_length) $excerpt .= '...';

			$excerpt .= ' <a href="' . get_the_permalink($id) . '">More Details</a>';

			return $excerpt;

		}

		static function get_any_image($post_id = null, $size = "medium", $html = false) {

			$fallback = get_field('default_image', 'option');

			$post_id = $post_id ? $post_id : get_the_ID();

			$people = das_people::get_participants($post_id);

			$post = get_post($post_id);

			if( get_the_post_thumbnail( $post_id, $size ) ) {

				//First try and get the thumbnail for the event
				$img = get_the_post_thumbnail_url( $post_id, $size );

			} elseif ( $people ) {

				$person = isset($people[0]["member"]) ? $people[0]["member"] : $people[0]["team"];
				$person = empty($person) ? $people[0]["individual"] : $person;

				$img = get_the_post_thumbnail_url( $person->ID, $size );

				if(!$img) {

					$all = das_people::get_all_participants( $post_id );

					foreach ($all as $p) {
						
						$img = get_the_post_thumbnail_url( $p->ID, 'medium' );

						if($img) break;
					}
				}

			}

			if(!$img) $img = $fallback;

			return ($html) ? '<img src="' . $img . '" alt="Picture of' . $post->post_title . '">' : $img;

		}

		static function build_cat_query($cat) {

			$venue = (isset($_GET["venue"])) ? "?venue[]=" . implode("&venue[]=", $_GET["venue"]) . "&" : "?";

			return $venue . "cat[]=" . $cat;

		}

		static function das_list_events($args) { 

			$id = (isset($args["id"])) ? $args["id"] : get_the_ID();

			$minimal = (isset($args["minimal"])) ? true : false;

			$minimal_class = $minimal ? ' minimal' : '';

			self::$cat = isset($args["cat"]) ? [$args["cat"]] : self::$cat;
			self::$venue = isset($args["venue"]) ? [$args["venue"]] : self::$venue;

			$events = self::group_tribe_events_by_day();

			$output = '<div class="das-list-events' . $minimal_class . '">';

			if(!$minimal) $output .= self::get_event_filters();

			foreach ($events as $key => $day) {

				$output .= '<div class="das-event-day border border-primary bg-muted rounded">

								<h2 class="das-event-day-header">' . self::format_day_header($key) . '</h2>
								<ul class="das-events-day">
								';

							foreach ($day as $event) {

								$cat = wp_get_post_terms($event->ID, 'tribe_events_cat')[0];

								$cost = tribe_get_cost($event->ID) ? '<p class="cost">Price: &euro;' . tribe_get_cost($event->ID) . '</p>' : '';

								$venue = tribe_get_venue($event->ID) ? ' @ <span class="venue">' . tribe_get_venue($event->ID) . '</span>' : '';

								$participants = das_people::list_event_participants($event->ID);

								$title = $event->post_title;

								$output .= '<li class="das-event ' . self::get_event_css_classes($event) . ' rounded">

												<div class="row">

													<div class="col-sm-3 event-image">' . self::get_any_image($event->ID, "medium", true) . '</div>

													<div class="col-sm-7">

														<h3 class="event-heading h5 font-weight-bold">

															<a href="' . $event->guid . '">
																<span class="date das-event-date">' . self::format_event_list_date($event->event_date) . '</span>

																<span class="das-event-header">' . $title . '</span>';

																if(strtolower($title) !== strtolower($participants)) $output .= ' / <span class="das-event-participants">' . $participants . '</span>';
																$output .= '
															</a></h3>

														<p class="das-event-category"><a href="' . self::build_cat_query($cat->term_taxonomy_id) . '">' . $cat->name . '</a>' . $venue . '</p>

														' . $cost . '

														<p class="event-blurb">' . self::get_excerpt($event->ID, 50) . '</p> 

														<div class="das-event-buttons">' . self::get_ticket_button_html($event->ID,$cat->name) . '</div>

													</div>

												</div>

											</li><!-- .das-event -->';

							}

				$output .= '</ul></div><!-- .das-event-day-->';
			}

			if(count($events) == 0) {
				$output .= '<div class="p-3 mb-2 bg-danger text-light">No events found, try <a href="?%3Fvenue%5B%5D%26cat%5B%5D" class="text-popping">Resetting the filters</a></div>';
			}

			$output .= '</div><!-- .das-list-events -->';

			return $output;

		} 
	}

}

?>