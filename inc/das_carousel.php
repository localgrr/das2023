<?php
if ( ! class_exists( 'das_carousel' ) ) {

	class das_carousel {

		private $carousel;

		const SIZES = ['small','medium','full'];
		
/**
 * Initial function run as the class is loaded
 *
 * This function fets the carousel data from ACF (if it exists)
 * or loads the featured image.
 *
 * @return void but echos the carousel
 * 
 */
		public function init() {

			$this->carousel = get_field('carousel');

			if(!$this->carousel) return false;

			if($this->carousel) {

				shuffle($this->carousel);
				$carousel = $this->get_carousel_html();

			} else {

				$carousel = $this->get_featured_image_html();

			}

			return $carousel;
		}

		public function __construct() {

			echo $this->init();

		}

/**
 * Builds the carousel HTML from te carousel array
 *
 * @global array $this->carousel the carousel array from ACF
 * @return string $ht The generated HTML content.
 * 
 */
		private function get_carousel_html() {

			$ht = '
			<div id="das-carousel" class="das-carousel carousel slide" data-ride="carousel">
				<h1 class="carousel-title display-2">' . get_the_title() . '</h1>
				<div class="carousel-inner">';

			foreach( $this->carousel as $i => $carousel_item) {

				$active = ($i==0) ? " active" : "";

				$ht .= '<div class="carousel-item' . $active . '">';
				$ht .= $this->get_featured_image_html($carousel_item);
				$ht .= '</div><!-- .carousel-item -->';
			}

			$ht .= '
					<div class="carousel-caption d-none d-md-block">
						<p>Photographer <a href="https://markanthonypratt.com" target="new">Mark Anthony Pratt</a></p>
					</div>
				</div><!-- .carousel-inner -->
			</div><!-- .carousel -->';

			return $ht;
 
		}

/**
 * Builds a static image container
 * 
 * it either builds from a wordpress image object, or if there is none uses the pages featured image
 *
 * @param WP_Image $timg_obj the carousel array from ACF
 * @return string $ht The generated HTML content.
 * 
 */
		private function get_featured_image_html($img_obj = false) {

			$ht = '
			<div class="main-image-container">';
			
			$ht .= !$img_obj ? '<h1 class="main-image-heading display-2">' . get_the_title() . '</h1>' : '';

			foreach (static::SIZES as $size) {

				$image = $img_obj ? wp_get_attachment_image($img_obj["ID"], $size) : get_the_post_thumbnail( get_the_id(), $size );

				$ht .= '<div class="featured-image i-' . $size . '">' . $image . '</div>';
				
			}

			$ht .= '</div>';

			return $ht;
			
		}

	}

}

?>